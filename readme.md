# Guia Basica Terminal

<div align="center"><img src="terminal-head.png" width="450px"></div>

## Comandos basicos

$ `ls` - Lista los ficheros

**Banderas del comando ls**

Las banderas se ponen a un lado del comando principal todas comienzan con un `-`.

- `-a` muestra los archivos ocultos.
- `-l` para mostrar usuario y permisos.

$ `pwd` - para saber en que directorio estas?.

$ `cd 'ruta del directorio'` - para cambiar de directorio.

$ `cd ..` - para retroceder un directorio.

$ `clear` Limpia la terminal.

## Crear y Copiar archivos

$ `mkdir 'Nombre de carpeta'` Para crear directorios/carpetas.

$ `rmdir 'Nombre de carpeta'` Para borrar directorios/carpetas.

$ `touch 'archivo.txt'` crea un archivo .txt con el nombre "archivo" .

**Nota:** este comando crea archivos en el directorio generado con la cualquier extension.

$ `cp 'archivo' 'directorio donde se copiará'` Copia un archivo al directorio seleccionado.

$ `mv 'archivo' 'directorio donde se copiará'` Mueve un archivo al directorio seleccionado.

$ `rm 'archivo'` borra un archivo del directorio seleccionado.

$ `rm -r 'ruta directorio'` borra un directorio con arhivos dentro.

$ `cat 'archivo'` Para ver el contenido de un archivo

$ `sudo` para entrar al modo **Super Usuario** (Pedirá la contraseña para entrar el modo **SU**)

$ `sudo 'Comando'` para ejecutar un comando en modo **Super Usuario** (Pedirá la contraseña para entrar el modo **SU**)

$ `exit` para cerrar el modo super usuario

---

<> with ❤️ by **Spectrasonic**
